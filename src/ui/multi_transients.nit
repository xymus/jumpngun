# This file is part of jumpngun by Alexis Laferrière
#
# To the extent possible under law, the person who associated CC0 with
# jumpngun has waived all copyright and related or neighboring rights
# to jumpngun.
#
# You should have received a copy of the CC0 legal code along with this
# work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.

# Identify transient objects that are passed once over the network
module multi_transients

import gamnit::network
import counter

intrude import game

redef class Object
	# Is `self` transient?
	#
	# A transient object does not have a reference id over the network.
	# This relieve pressure on the serialization cache by not filling
	# it with object that passed use once and never referred to again.
	#
	# It can also be used to lose the identity of an object over the network
	# so the other end recretes it as different instances.
	#
	# Transient object must not have cycles.
	fun transient: Bool do return false
end

redef class SerializerCache
	# Debug service printing the kind of objects cached by `self`
	fun print_stats do end
end

redef class AsyncCache
	redef fun []=(id, object)
	do
		if object.transient then return
		super
	end

	redef fun has_object(object) do return not object.transient and super

	redef fun new_id_for(object)
	do
		if object.transient then return next_available_id
		return super
	end

	redef fun print_stats do
		var counter = new Counter[String]
		for val in received.values do counter.inc val.class_name
		counter.print_elements(32)
	end
end

redef class Bullet
	redef fun transient do return true
end

redef class Point
	redef fun transient do return true
end

redef class Array
	redef fun transient do return true
end
