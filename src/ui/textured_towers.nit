# This file is part of jumpngun by Alexis Laferrière
#
# To the extent possible under law, the person who associated CC0 with
# jumpngun has waived all copyright and related or neighboring rights
# to jumpngun.
#
# You should have received a copy of the CC0 legal code along with this
# work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.

# Pretty textured towers
module textured_towers

import jumpngun

redef class App

	private var wall_textures: Array[Texture] = [
		new Texture("textures/wall0.png"),
		new Texture("textures/wall1.png"),
		new Texture("textures/wall2.png"),
		new Texture("textures/wall3.png")]
	private var wall_speculars: Array[Texture] = [
		new Texture("textures/wall0_spec.png"),
		new Texture("textures/wall1_spec.png"),
		new Texture("textures/wall2_spec.png"),
		new Texture("textures/wall3_spec.png")]
	private var wall_bumps: Array[Texture] = [
		new Texture("textures/wall0_bump.png"),
		new Texture("textures/wall1_bump.png"),
		new Texture("textures/wall2_bump.png"),
		new Texture("textures/wall3_bump.png")]

	private var roof_texture = new Texture("textures/roof.png")
	private var roof_specular = new Texture("textures/roof.png")
	private var roof_bump = new Texture("textures/roof.png")

	private var goal_texture = new Texture("textures/gold.png")
	private var goal_specular = new Texture("textures/gold_spec.png")
	private var goal_bump = new Texture("textures/gold_spec.png")

	private var spawn_texture = new Texture("textures/bronze.png")
	private var spawn_specular = new Texture("textures/bronze_spec.png")
	private var spawn_bump = new Texture("textures/bronze_spec.png")

	redef fun on_create
	do
		super

		# Generate mipmap for tower textures for a nicer look from afar
		for root in all_root_textures do
			var gl_name = root.gl_texture
			if gl_name == 0 then continue # Skip invalid textures

			glBindTexture(gl_TEXTURE_2D, root.gl_texture)
			glTexParameteri(gl_TEXTURE_2D, gl_TEXTURE_MIN_FILTER, gl_LINEAR_MIPMAP_LINEAR)
			glGenerateMipmap gl_TEXTURE_2D
		end
	end
end

redef class Tower

	redef var model = new TowerModel(self)

	redef fun create_ui do app.actors.add actor

	# Mesh for the walls only, no top nor bottom
	fun to_walls: TowerWalls
	do
		var width = right.to_f-left.to_f
		var height = top.to_f-bottom.to_f
		var depth = front.to_f-back.to_f
		return new TowerWalls(width, height, depth, goal or spawn)
	end

	# Roof mesh
	fun to_roof: TowerRoof
	do
		var width = right.to_f-left.to_f
		var height = top.to_f-bottom.to_f
		var depth = front.to_f-back.to_f
		return new TowerRoof(width, height, depth, goal or spawn)
	end
end

# Model for a single `tower`, with walls and roof as leaves
class TowerModel
	super Model

	# Represented logical entity
	var tower: Tower

	redef var leaves = [
		new LeafModel(tower.to_walls, material(true)),
		new LeafModel(tower.to_roof, material(false))] is lazy

	private fun material(wall: Bool): Material
	do
		# Color
		var mat
		if tower.goal or tower.spawn then
			var c = 0.35
			mat = new TexturedMaterial([c, c, c, 1.0], [1.0]*4, [1.0]*4)
		else
			var c = 0.4 & 0.15
			mat = new TexturedMaterial([c, c, c, 1.0], [c, c, c, 1.0], [1.0]*4)
		end

		# Texture
		if tower.goal then
			mat.diffuse_texture = app.goal_texture
			mat.ambient_texture = app.goal_texture
			mat.specular_texture = app.goal_specular
			mat.normals_texture = app.goal_bump
		else if tower.spawn then
			mat.diffuse_texture = app.spawn_texture
			mat.ambient_texture = app.spawn_texture
			mat.specular_texture = app.spawn_specular
			mat.normals_texture = app.spawn_bump
		else if wall then
			var i = app.wall_textures.length.rand
			mat.ambient_texture = app.wall_textures[i]
			mat.specular_texture = app.wall_speculars[i]
			mat.normals_texture = app.wall_bumps[i]
		else # roof
			mat.ambient_texture = app.roof_texture
			mat.specular_texture = app.roof_specular
			mat.normals_texture = app.roof_bump
		end

		return mat
	end
end

# ---
# Meshes, derived from `Cuboid` from the gamnit sources.
# Each class shows only some of the faces and some textures are repeated.

# Tower walls mesh
class TowerWalls
	super Cuboid

	# Is this the goal tower?
	var goal: Bool

	redef var vertices is lazy do
		var a = [-0.5*width, -0.5*height, -0.5*depth]
		var b = [ 0.5*width, -0.5*height, -0.5*depth]
		var c = [-0.5*width,  0.5*height, -0.5*depth]
		var d = [ 0.5*width,  0.5*height, -0.5*depth]

		var e = [-0.5*width, -0.5*height,  0.5*depth]
		var f = [ 0.5*width, -0.5*height,  0.5*depth]
		var g = [-0.5*width,  0.5*height,  0.5*depth]
		var h = [ 0.5*width,  0.5*height,  0.5*depth]

		var vertices = new Array[Float]
		for v in [a, c, d, a, d, b, # front
		          f, h, g, f, g, e, # back
		          b, d, h, b, h, f, # right
		          e, g, c, e, c, a  # left
				  ] do vertices.add_all v
		return vertices
	end

	redef var normals is lazy do
		var normals = new Array[Float]
		var faces_normals = [
			[0.0, 0.0, -1.0],
			[0.0, 0.0,  1.0],
			[ 1.0, 0.0, 0.0],
			[-1.0, 0.0, 0.0]]
		for f in faces_normals do for i in 6.times do normals.add_all f
		return normals
	end

	redef var texture_coords: Array[Float] is lazy do
		var w = 1.0
		var h = height*0.075

		if not goal then
			# Repeat texture when not the goal tower
			w *= 8.0
			h *= 8.0
		end

		var a = [0.0, h]
		var b = [w,   h]
		var c = [0.0, 0.0]
		var d = [w,   0.0]

		var texture_coords = new Array[Float]
		var face = [a, c, d, a, d, b]
		for i in 4.times do for v in face do texture_coords.add_all v
		return texture_coords
	end

	redef var center = new Point3d[Float](0.0, 0.0, 0.0) is lazy
end

# Tower roof mesh
class TowerRoof
	super Cuboid

	# Is this the goal tower?
	var goal: Bool

	redef var vertices is lazy do
		var c = [-0.5*width,  0.5*height, -0.5*depth]
		var d = [ 0.5*width,  0.5*height, -0.5*depth]
		var g = [-0.5*width,  0.5*height,  0.5*depth]
		var h = [ 0.5*width,  0.5*height,  0.5*depth]

		var vertices = new Array[Float]
		for v in [c, g, h, c, h, d  # top
				  ] do vertices.add_all v
		return vertices
	end

	redef var normals is lazy do
		var normals = new Array[Float]
		var faces_normals = [
			[0.0,  1.0, 0.0]]
		for f in faces_normals do for i in 6.times do normals.add_all f
		return normals
	end

	redef var texture_coords: Array[Float] is lazy do
		var w = width*0.5
		var h = depth*0.5
		if goal then
			w = 1.0
			h = 1.0
		end
		var a = [0.0, h]
		var b = [w,   h]
		var c = [0.0, 0.0]
		var d = [w,   0.0]

		var texture_coords = new Array[Float]
		var face = [a, c, d, a, d, b]
		for i in 1.times do for v in face do texture_coords.add_all v
		return texture_coords
	end

	redef var center = new Point3d[Float](0.0, 0.0, 0.0) is lazy
end
