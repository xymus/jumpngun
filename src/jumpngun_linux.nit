# This file is part of jumpngun by Alexis Laferrière
#
# To the extent possible under law, the person who associated CC0 with
# jumpngun has waived all copyright and related or neighboring rights
# to jumpngun.
#
# You should have received a copy of the CC0 legal code along with this
# work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.

# Linux variant
module jumpngun_linux

import linux
intrude import linux::audio

import jumpngun
import textured_towers

redef class App
	redef fun accept_event(event)
	do
		if event isa GamnitPointerEvent then
			var sdl_event = event.native
			if sdl_event isa SDLMouseButtonEvent and sdl_event.button == 3 then
				sniper_mode = sdl_event isa SDLMouseButtonDownEvent
				return true
			else if sdl_event isa SDLMouseMotionEvent then
				var sensivity = 0.4
				if sniper_mode then sensivity *= 0.4
				world_camera.turn(sdl_event.xrel.to_f*sensivity, sdl_event.yrel.to_f*sensivity)
				return true
			end
		end

		return super
	end

	# Sniper mode, zoomed in
	var sniper_mode = false is private writable(set_sniper_mode)

	# Sniper mode, zoomed in
	fun sniper_mode=(val: Bool)
	do
		if val then
			field_of_view_y_bk = world_camera.field_of_view_y
			world_camera.field_of_view_y = 0.2
		else
			world_camera.field_of_view_y = field_of_view_y_bk
		end

		set_sniper_mode val
	end

	private var field_of_view_y_bk = 0.0
end

redef class Sound
	redef fun load
	do
		super

		var chunk = native
		if chunk != null then mix.volume_chunk(chunk, 25)
	end
end
