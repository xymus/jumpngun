# This file is part of jumpngun by Alexis Laferrière
#
# To the extent possible under law, the person who associated CC0 with
# jumpngun has waived all copyright and related or neighboring rights
# to jumpngun.
#
# You should have received a copy of the CC0 legal code along with this
# work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.

# Player and character
module characters is serialize

import physics

redef class Level
	# Live characters (only `player` at this time)
	var characters = new BoxedArray[Character]

	redef fun update(dt)
	do
		super

		for c in characters do
			bk.fill_from c.position

			# Do logic
			c.update dt

			# Detect collision!
			var hit = towers.items_overlapping(c)
			if hit.not_empty then
				# Win condition
				var tower = hit.first
				if tower.goal and c.position.y >= tower.platform_center.y + 1.0 then
					c.won = true
				end

				# On the ground? Try moving sideways only without falling
				c.position.y = bk.y
				c.speed.y = 0.0
				hit = towers.items_overlapping(c)
				c.air = false

				if hit.not_empty then
					# The was a hit on the side, grab a tower wall or something like that
					c.position.fill_from bk
				end

				# Stick to the floor
				c.speed.x = 0.0
				c.speed.z = 0.0
			else
				# In the air
				c.air = true

				# Air friction
				var conserve = 0.98
				c.speed.x *= conserve
				c.speed.z *= conserve
			end
		end
	end
	private var bk = new Point3d[Float] is noserialize
end

# Player character, where `position` is at the center of the head
class Character
	super PhysicsEntity

	init do acceleration.y = gravity

	# Is this character currently alive?
	var alive = true

	# Has this player reached to goal tower?
	var won = false

	# Position of the head, alias to `position`
	fun head: Point3d[Float] do return position

	redef fun create
	do
		super
		level.characters.add self
	end

	redef fun destroy
	do
		super
		level.characters.remove self
		alive = false
	end

	# Reset all attributes (and more) to their default values
	#
	# May be used after moving a `Character` to a new `Level`.
	fun reset(new_level: Level)
	do
		level = new_level
		won = false
		alive = true

		speed.x = 0.0
		speed.y = 0.0
		speed.z = 0.0

		if not level.characters.has(self) then
			level.characters.add self
			level.physic_entities.add self
		end
	end

	# Width in meters of the hitbox
	var width =  0.4

	# Height in meters of the hitbox
	var height = 1.8

	# Depth in meters of the hitbox
	var depth =  0.4

	redef fun left do return head.x - width*0.5
	redef fun right do return head.x + width*0.5
	redef fun top do return head.y + height*0.25
	redef fun bottom do return head.y - height*0.75
	redef fun front do return head.z + depth*0.5
	redef fun back do return head.z - depth*0.5
end
