# This file is part of jumpngun by Alexis Laferrière
#
# To the extent possible under law, the person who associated CC0 with
# jumpngun has waived all copyright and related or neighboring rights
# to jumpngun.
#
# You should have received a copy of the CC0 legal code along with this
# work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.

# Gun and bullets
module guns is serialize

import characters

redef class Level

	# Live bullets
	var bullets = new Array[Bullet]

	# Entities that can be hit by a bullet (not including towers)
	var hittables = new BoxedArray[PhysicsEntity]

	private var update_clock = new Clock is noserialize
	redef fun update(dt)
	do
		super

		update_clock.lapse
		for b in bullets.reverse_iterator do

			last_bullet_position.fill_from b.position
			b.update dt

			# Try to hit a `PhysicsEntity`
			var hit_entity = hittables.items_overlapping(b)
			if (hit_entity.length == 1 and hit_entity.first != b.origin) or
			   hit_entity.length > 1 then

				# explode
				b.hit(hit_entity.first, last_bullet_position)
				b.destroy
				continue
			end

			# Try to hit towers
			var hit = towers.items_overlapping(b)
			if hit.not_empty then
				# explode
				b.hit(hit.first, last_bullet_position)
				b.destroy
				continue
			end

			# clean up out of range bullet
			var far = radius
			if b.position.x > far or b.position.x < -far or
			   b.position.y > far or b.position.y < -far or
			   b.position.z > far or b.position.z < -far then
				b.destroy
				continue
			end
		end
		perfs["gun bullets"].add update_clock.lapse
	end
	private var last_bullet_position = new Point3d[Float] is noserialize
end

redef class PhysicsEntity
	redef fun create
	do
		super
		if not self isa Bullet then level.hittables.add self
	end

	redef fun destroy
	do
		super
		if not self isa Bullet then level.hittables.remove self
	end
end

redef class Character
	# Held weapon
	var gun = new Gun

	# Try to open fire with `gun`
	fun try_to_fire do gun.try_to_fire(self)

	redef fun update(dt)
	do
		super
		gun.update dt
	end

	redef fun reset(new_level)
	do
		super
		if not level.hittables.has(self) then
			level.hittables.add self
		end
	end
end

# Logical entity fireing bullets
class Gun
	super Updatable

	# Cooldown time after opening fire
	var max_cooldown = 0.8

	# Time remaining until it's possible to open fire
	var cooldown: Float = max_cooldown.rand

	# Directionnal vector of the direction where bullets go
	var orientation = new Point3d[Float]

	redef fun update(dt)
	do
		cooldown = 0.0.max(cooldown-dt)
	end

	# Try to open fire by `shooter`
	fun try_to_fire(shooter: PhysicsEntity)
	do
		if cooldown > 0.0 then return
		open_fire shooter
	end

	# Open fire from `shooter` (should be called via `try_to_fire`)
	protected fun open_fire(shooter: PhysicsEntity): Bullet
	do
		cooldown = max_cooldown

		var bullet = create_bullet(shooter)

		# Set same position as the shooter
		bullet.position.fill_from shooter.position

		# From the gun
		# TODO
		if shooter isa Character then bullet.position.y -= 0.4

		# Push forward
		var s = 32.0
		bullet.speed.x += orientation.x*s
		bullet.speed.y += orientation.y*s
		bullet.speed.z += orientation.z*s

		bullet.create
		return bullet
	end

	private fun create_bullet(shooter: PhysicsEntity): Bullet
	do return new ExplosiveBullet(shooter.level, shooter, self)
end

# Abstract bullet
abstract class Bullet
	super CubicEntity

	# Origin/shooter which can't be collided with
	var origin: PhysicsEntity

	# Origin gun that shot `self`
	var gun: Gun

	redef var dim = 0.1

	# Hit `entity` at approximately `pos`
	protected fun hit(entity: Boxed3d[Float], pos: Point3d[Float]) do end

	redef fun create
	do
		super
		level.bullets.add self
	end

	redef fun destroy
	do
		super
		level.bullets.remove self
	end
end

# Bullets exploding on contaxt
class ExplosiveBullet
	super Bullet

	# Explosion force, applied to entities at 1.0 m
	var force = 48.0

	redef fun hit(entity, pos) do level.explode(pos, force)
end
