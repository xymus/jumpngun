# This file is part of jumpngun by Alexis Laferrière
#
# To the extent possible under law, the person who associated CC0 with
# jumpngun has waived all copyright and related or neighboring rights
# to jumpngun.
#
# You should have received a copy of the CC0 legal code along with this
# work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.

# Gun and shield turrets
module turrets is serialize

import physics
import characters
import guns

redef class Level
	# Live turrets
	var turrets = new Array[Turret]

	# Live shield turrets
	var shield_turrets = new Array[ShieldTurret]

	redef fun generate
	do
		super

		goal_tower.add_turret self

		# Add turrents or turret complex to half of the towers
		var n_turrets = towers.length / 2 + 2
		if args.has("--small") then n_turrets = 4
		var shuffled = towers.sample(n_turrets)
		shuffled.remove_all goal_tower
		shuffled.remove_all spawn_tower

		var complexes_built = 0
		var complexes_max = 4

		for tower in shuffled do
			var b = 10.0
			if tower.width > b and tower.depth > b and complexes_built < complexes_max then
				complexes_built += 1
				tower.add_turret_complex self
			else tower.add_turret self
		end

		# Add shields
		for turret in shield_turrets do turret.create_shield
	end

	redef fun update(dt)
	do
		super
		for turret in turrets do turret.update(dt)
	end
end

redef class Tower

	# Add a single turret at the top of this tower
	private fun add_turret(level: Level)
	do
		var turret = new GunTurret(level, platform_center)
		level.turrets.add turret
	end

	# Add multiple turrets at the top of this tower (incl. a shield turret)
	private fun add_turret_complex(level: Level)
	do
		var n = 3
		var d = 4.0
		var oa = 2.0*pi.rand
		for i in n.times do
			var a = i.to_f/n.to_f * 2.0 * pi + oa
			var p = new Point3d[Float]
			p.fill_from platform_center
			p.x += d * a.cos
			p.z += d * a.sin
			var turret = new GunTurret(level, p)
			level.turrets.add turret
		end

		var turret = new ShieldTurret(level, platform_center)
		level.turrets.add turret
		level.shield_turrets.add turret
	end
end

# Logical turret, only the parts are physical/world entities
abstract class Turret
	super Updatable

	# World
	var level: Level

	# Center of the bottom of the turret
	var floor_center: Point3d[Float]

	# Is this turret still active/working/alive?
	var active = true

	# Live parts, includes `base` and `head`
	var parts = new Array[TurretPart]

	# Base part, static and on the ground
	var base: TurretBase is noinit

	# Main head part, may turn but should not move
	var head: TurretHead is noinit
end

# Component of a `Turret` responding to the physics engine
abstract class TurretPart
	super CubicEntity

	# Parent turret
	var turret: Turret

	redef fun destroy
	do
		super
		turret.parts.remove self
		turret.active = false
	end
end

# Moving upper part of a `Turret`
abstract class TurretHead
	super TurretPart

	# Rotation around the X axis
	var pitch = 0.0

	# Rotation around the Y axis
	var yaw: Float = 2.0*pi.rand

	redef var dim = 2.0

	redef fun pushed(from, relative_force)
	do
		acceleration.y = gravity
		super
	end
end

# Static lower part of a `Turret`
abstract class TurretBase
	super TurretPart

	redef var dim = 0.05

	redef fun pushed(from, relative_force)
	do
		destroy
	end
end

# ---
# Gun turret

# Lower part of a `GunTurret`
class GunTurretBase super TurretBase end

# Upper part of a `GunTurret`
class GunTurretHead super TurretHead end

# Turret targeting the player
class GunTurret
	super Turret

	# Weapon
	var gun = new Gun

	private var last_target: nullable Character = null

	init
	do
		var head = new GunTurretHead(level, self)
		head.position.fill_from floor_center
		head.position.y += 1.0
		head.create
		parts.add head
		self.head = head

		var base = new GunTurretBase(level, self)
		base.position.fill_from floor_center
		base.create
		parts.add base
		self.base = base
	end

	redef fun update(dt)
	do
		super

		head.update dt
		gun.update dt

		if not active then return

		# Track a target
		var target = last_target
		if target == null or not valid_target(target) then
			target = null
			for character in level.characters do
				if valid_target(character) then
					target = character
					break
				end
			end
		end

		if target == null then
			head.yaw += dt
			return
		end

		last_target = target

		# Follow the target
		var rspeed = dt*2.0
		var target_pitch = head.position.pitch(target.position)
		head.pitch = rspeed.angle_lerp(head.pitch, target_pitch)
		var target_yaw = head.position.atan2_xz(target.position)
		head.yaw = rspeed.angle_lerp(head.yaw, target_yaw)

		gun.orientation.from_euler_angles(head.pitch, head.yaw)
		if head.pitch >= 0.0  then
			# Shoot if not dangerous
			gun.try_to_fire head
		end
	end

	private fun valid_target(character: Character): Bool
	do
		var d = head.position.dist2(character.position)

		# Don't follow if too far away
		return d < 48.0*48.0 and d > 9.0
	end
end

# ---
# Shield turret

# Lower part of a `ShieldTurret`
class ShieldTurretBase super TurretBase end

# Upper part of a `ShieldTurret`
class ShieldTurretHead
	super TurretHead

	# Index of this head among other heads
	var n: Int

	redef fun update(dt)
	do
		super
		yaw += dt*(1.8-0.3*n.to_f)*pi
	end
end

# Turret projecting a shield
class ShieldTurret
	super Turret

	# Radius of the shield blocking incoming bullets
	var shield_radius = 15.0

	init
	do
		for i in 3.times do
			var head = new ShieldTurretHead(level, self, i)
			head.position.fill_from floor_center
			head.position.y += 2.0
			head.yaw = 2.0*pi.rand
			head.create
			parts.add head
		end
		self.head = parts.first.as(ShieldTurretHead)

		var base = new ShieldTurretBase(level, self)
		base.position.fill_from floor_center
		base.create
		parts.add base
		self.base = base
	end

	# Create the shield in the UI
	fun create_shield do end

	redef fun update(dt)
	do
		super

		for part in parts do part.update dt

		if not active then return
	end
end

redef class Bullet
	private var last_bullet_position = new Point3d[Float]

	redef fun update(dt)
	do
		last_bullet_position.fill_from position

		super

		for turret in level.shield_turrets do if turret.active then
			var prev_d = last_bullet_position.dist(turret.head.position)
			var d = position.dist(turret.head.position)

			var r = turret.shield_radius
			if prev_d > r and d <= r then
				destroy
				hit(self, last_bullet_position)
				level.bullets.remove self
			end
		end
	end
end

redef class ExplosiveBullet
	redef fun hit(entity, pos)
	do
		# Avoid friendly fire on turrets, instead kill the bullet silently
		if entity isa ShieldTurretHead and origin isa GunTurretHead then return

		super
	end
end
