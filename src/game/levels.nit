# This file is part of jumpngun by Alexis Laferrière
#
# To the extent possible under law, the person who associated CC0 with
# jumpngun has waived all copyright and related or neighboring rights
# to jumpngun.
#
# You should have received a copy of the CC0 legal code along with this
# work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.

# Level/world structure and generation
module levels is serialize

import game_core

# Root world object
class Level
	super Updatable

	# Towers
	var towers = new BoxedArray[Tower]

	# Starting tower
	var spawn_tower: Tower is noinit

	# End tower, goal to win
	var goal_tower: Tower is noinit

	# Approximate upper bound to the world radius
	var radius = 200.0

	# Generate world before play
	fun generate
	do
		var mod = if args.has("--small") then 0.1 else 1.0

		# Spawn
		var ox = -160.0*mod
		var oz = -70.0*mod

		# Goal
		var dx = 160.0*mod
		var dz = 70.0*mod

		# Handle
		var hmod = (2.rand*2-1).to_f*(50.0+200.0.rand)
		var hx =  hmod
		var hz = -hmod

		# Spawn and goal
		#            X     Y     Z     w     d
		add_tower(  ox,  0.0,   oz,  8.0,  8.0, spawn=true)
		add_tower(  dx,  4.0,   dz,  6.0,  6.0, goal=true)

		var n_tower = 32
		var r = 20.0

		var towers = towers
		while towers.length < n_tower do
			var p = 1.0.rand
			var x = p.qerp(ox, hx, dx) & r
			var z = p.qerp(oz, hz, dz) & r
			var y = -16.0.rand

			var w = 6.0+4.0.rand.pow(2.0)
			var d = 6.0+4.0.rand.pow(2.0)
			if not add_tower(x, y, z, w, d) then r += 1.0
		end
	end

	private fun add_tower(x, y, z, width, depth: Float, goal, spawn: nullable Bool): Bool
	do
		var t = new Tower(new Point3d[Float](x, y, z),
		                  width, 120.0+y, depth,
		                  goal or else false, spawn or else false)

		var hit = towers.items_overlapping(t)
		if hit.not_empty then return false

		if spawn == true then spawn_tower = t
		if goal == true then goal_tower = t
		towers.add t
		t.create_ui
		return true
	end
end

# Tower acting as platform
class Tower
	super Boxed3d[Float]

	autoinit(platform_center, width, height, depth, goal, spawn)

	# Center of the top platform/roof
	var platform_center: Point3d[Float]

	# Width, in meters
	var width: Float

	# Height, in meters
	var height: Float

	# Depth, in meters
	var depth: Float

	# Is this the end/goal to reach?
	var goal: Bool

	# Is this the starting tower?
	var spawn: Bool

	private fun pc: Point3d[Float] do return platform_center # alias
	redef var left = pc.x - width*0.5 is lateinit
	redef var right = pc.x + width*0.5 is lateinit
	redef var top = pc.y is lateinit
	redef var bottom = pc.y - height is lateinit
	redef var front = pc.z + depth*0.5 is lateinit
	redef var back = pc.z - depth*0.5 is lateinit

	# Hook to create `self` in the UI
	fun create_ui do end
end
