# This file is part of jumpngun by Alexis Laferrière
#
# To the extent possible under law, the person who associated CC0 with
# jumpngun has waived all copyright and related or neighboring rights
# to jumpngun.
#
# You should have received a copy of the CC0 legal code along with this
# work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.

# Core services
module game_core

import geometry
import performance_analysis
import serialization

# Object updated once per frame by its parent, from the root in `Level`
abstract class Updatable

	# Update game logic for the last `dt` seconds
	fun update(dt: Float) do end
end

redef class Point3d[N]
	# Copy coordinates from `other` into `self`
	fun fill_from(other: Point3d[N])
	do
		x = other.x
		y = other.y
		z = other.z
	end

	# Fill as a directional vector from euler angles
	fun from_euler_angles(pitch, yaw: Float)
	do
		x = -yaw.sin * pitch.cos
		y = pitch.sin
		z = -pitch.cos * yaw.cos
	end

	# Arctangent function using the difference between `self` and `other` as trigonometric ratio on the XZ plane
	#
	# Behave similarly to the toplevel `atan2` as it returns the angle in the appropriate quadrant.
	fun atan2_xz(other: Point3d[N]): Float
	do
		var dx = other.x.to_f - x.to_f
		var dz = other.z.to_f - z.to_f
		return sys.atan2(-dx.to_f, -dz.to_f)
	end

	# Distance between `self` and `other` projected on the XZ plane
	fun dist_xz(other: Point3d[N]): Float
	do
		var dx = other.x.to_f - x.to_f
		var dz = other.z.to_f - z.to_f
		return (dz*dz + dx*dx).sqrt
	end

	# Angle around X between `self` and `other`
	fun pitch(other: Point3d[N]): Float
	do
		var dxz = dist_xz(other)
		var dy = other.y.to_f - y.to_f
		return sys.atan2(dy.to_f, dxz.to_f)
	end
end

redef class Float
	# Fuzzy value in `[self-variation..self+variation]`
	fun &(variation: Float): Float do return self - variation + 2.0*variation.rand
end
