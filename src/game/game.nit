# This file is part of jumpngun by Alexis Laferrière
#
# To the extent possible under law, the person who associated CC0 with
# jumpngun has waived all copyright and related or neighboring rights
# to jumpngun.
#
# You should have received a copy of the CC0 legal code along with this
# work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.

# Whole game logic
module game

import levels
import physics
import characters
import jetpack
import guns
import turrets

# Factory creating a game, refined in `ui/multi.nit`
#
# https://github.com/nitlang/nit/issues/1279
fun new_game_context: GameContext
do
	var level = new Level
	level.generate
	return new GameContext(level)
end

# Game context, wraps a `level` and  belongs to a single `player`
class GameContext
	super Updatable

	# World/game/level
	var level: Level is writable(internal_level=), noinit

	# World/game/level
	fun level=(new_level: Level) is autoinit do
		if isset _level then cleanup_old_level level
		internal_level = new_level
		install_new_level new_level
	end

	# Local player character
	var player = new Character(level) is lazy

	# Additional preparation after `level` was fully instantiated
	#
	# This is used by `src/ui/multi.nit` to add game entities to the UI.
	fun prepare do end

	# Spawn `player` on top of `spawn_tower` or respawn if `not player.alive`
	fun spawn_player
	do
		var player = player
		if not level.characters.has(player) then player.create
		player.reset level

		player.position.fill_from level.spawn_tower.platform_center
		player.position.y += 2.0
	end

	redef fun update(dt)
	do
		level.update dt
	end

	# Get a new `level`
	fun new_level
	do
		var level = new Level
		level.generate
		self.level = level
	end

	# Clean up data and UI related to `old_level`
	protected fun cleanup_old_level(old_level: Level) do end

	# Prepare UI for the `new_level`
	protected fun install_new_level(new_level: Level) do end
end
