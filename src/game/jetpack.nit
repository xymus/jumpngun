# This file is part of jumpngun by Alexis Laferrière
#
# To the extent possible under law, the person who associated CC0 with
# jumpngun has waived all copyright and related or neighboring rights
# to jumpngun.
#
# You should have received a copy of the CC0 legal code along with this
# work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.

# Jetpack logic
module jetpack

import characters

redef class Character

	# Maximum fuel capacity
	var max_fuel = 10.0

	# Current fuel, out of `max_fuel`
	var fuel: Float = max_fuel

	# Is the jetpack actually active?
	var jetpack_active = false

	# Is the player trying to use the jetpack?
	var jetpack_try_active = false

	private var fuel_consumption_per_second: Float = -10.0
	private var fuel_generation_per_second = 16.0
	private var fuel_generation_per_second_air = 0.5

	redef fun update(dt)
	do
		super

		if not jetpack_active then
			var fgps = if air then
					fuel_generation_per_second_air
				else fuel_generation_per_second

			fuel = max_fuel.min(fuel + dt*fgps)
		else
			jetpack_active = false
			acceleration.y = gravity
		end
		jetpack_try_active = false
	end

	# Turn on the jetpack for `dt` seconds
	fun activate_jetpack(dt: Float)
	do
		jetpack_try_active = true

		if fuel <= 0.01 then return

		var secs_left = -fuel/fuel_consumption_per_second
		dt = dt.min(secs_left)

		acceleration.y += 32.0
		fuel = 0.0.max(fuel + dt*fuel_consumption_per_second)
		jetpack_active = true
	end

	# Jump off the ground
	fun jump
	do
		speed.y += 4.0
	end
end
