all: bin/jumpngun bin/jumpngun_multi

bin/jumpngun: $(shell nitls -M src/jumpngun_linux.nit)
	mkdir -p bin/
	nitc src/jumpngun_linux.nit -o $@

bin/jumpngun_multi: $(shell nitls -M src/jumpngun_linux.nit -m src/ui/multi.nit)
	mkdir -p bin/
	nitc src/jumpngun_linux.nit -o $@ -m src/ui/multi.nit

android: bin/jumpngun.apk
bin/jumpngun.apk: $(shell nitls -M src/jumpngun.nit -m android -m multi)
	mkdir -p bin/
	nitc src/jumpngun.nit -m android -o $@ -m src/ui/multi.nit

check:
	nitunit .

clean:
	rm -rf bin/
